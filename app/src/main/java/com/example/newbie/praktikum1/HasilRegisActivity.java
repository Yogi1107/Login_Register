package com.example.newbie.praktikum1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class HasilRegisActivity extends AppCompatActivity {


    private TextView txt1, txt2, txt3, txt4, txt5, txt6;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hasil_regis);

        txt1 = findViewById(R.id.terima_nama);
        txt2 = findViewById(R.id.terima_alamat);
        txt3 = findViewById(R.id.terima_no);
        txt4 = findViewById(R.id.terima_email);
        txt5 = findViewById(R.id.terima_jk);
        txt6 = findViewById(R.id.terima_hobi);


        Intent i = getIntent();
        txt1.setText(i.getStringExtra("nama"));
        txt2.setText(i.getStringExtra("alamat"));
        txt3.setText(i.getStringExtra("no"));
        txt4.setText(i.getStringExtra("email"));
        txt5.setText(i.getStringExtra("jk"));
        txt6.setText(i.getStringExtra("hobi"));
    }
}
